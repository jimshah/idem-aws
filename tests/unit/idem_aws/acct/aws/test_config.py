import tempfile
from unittest import mock

import pytest
import yaml

ACCT = """
aws:
  default:
    aws_access_key_id: my_key_id
    aws_secret_access_key: my_secret_key
    # region_name
"""

PROFILES = dict(profiles=yaml.safe_load(ACCT))

CONFIG = b"""
acct:
  extras:
    aws:
      region_name: mock-region
"""


@pytest.mark.asyncio
async def test_gather(hub):
    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as fh:
        fh.write(CONFIG)
        fh.flush()

        with mock.patch("sys.argv", ["idem", "state", f"--config={fh.name}"]):
            hub.pop.config.load(
                ["idem", "acct", "rend", "evbus"], "idem", parse_cli=True
            )

    assert hub.OPT.acct.extras == {"aws": {"region_name": "mock-region"}}

    ctx = await hub.idem.acct.ctx(
        path="states.aws.", acct_profile="default", acct_data=PROFILES
    )

    assert ctx.acct.get("region_name") == "mock-region", hub.OPT.acct.extras
