def test_build_arn(hub):
    arn = hub.tool.aws.test_state = hub.tool.aws.arn_utils.build_arn(
        partition="fakepartition",
        service="fakeservice",
        region="fakeregion",
        account_id="fakeaccountid",
        resource="fakeresource",
    )
    assert "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresource" == arn

    arn = hub.tool.aws.test_state = hub.tool.aws.arn_utils.build_arn(
        partition="fakepartition",
        service="fakeservice",
        account_id="fakeaccountid",
        resource="fakeresource",
    )
    assert "arn:fakepartition:fakeservice::fakeaccountid:fakeresource" == arn

    arn = hub.tool.aws.test_state = hub.tool.aws.arn_utils.build_arn(
        partition="fakepartition",
        service="fakeservice",
        region="fakeregion",
        resource="fakeresource",
    )
    assert "arn:fakepartition:fakeservice:fakeregion::fakeresource" == arn

    arn = hub.tool.aws.test_state = hub.tool.aws.arn_utils.build_arn(
        partition="fakepartition",
        service="fakeservice",
        region="fakeregion",
        account_id="fakeaccountid",
    )
    assert "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:" == arn

    arn = hub.tool.aws.test_state = hub.tool.aws.arn_utils.build_arn(
        partition="fakepartition",
        service="fakeservice",
        region="fakeregion",
    )
    assert "arn:fakepartition:fakeservice:fakeregion::" == arn

    arn = hub.tool.aws.test_state = hub.tool.aws.arn_utils.build_arn(
        partition="fakepartition",
        service="fakeservice",
    )
    assert "arn:fakepartition:fakeservice:::" == arn
