import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_account(hub, ctx, aws_organization, aws_organization_unit):
    expected_account_name = "idem-test-account" + str(uuid.uuid4())
    expected_email = "idem-test-email-" + str(uuid.uuid4()) + "@example.com"
    expected_role_name = "idem-test-aws-role-" + str(uuid.uuid4())
    expected_iam_user_access_to_billing = "ALLOW"
    expected_parent_id = aws_organization_unit.get("resource_id", None)
    expected_tags = [{"Key": "org", "Value": "idem"}, {"Key": "env", "Value": "test"}]

    # Create account with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    present_ret = await hub.states.aws.organizations.account.present(
        test_ctx,
        name=expected_account_name,
        email=expected_email,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        parent_id=expected_parent_id,
        tags=expected_tags,
    )
    new_state = present_ret.get("new_state")
    old_state = present_ret.get("old_state")

    assert new_state and not old_state
    assert (
        f"Would create aws.organizations.account {expected_account_name}"
        in present_ret["comment"]
    )

    assert (
        new_state["name"] == expected_account_name
    ), "Expected and Actual account_name should be equal"
    assert (
        new_state["email"] == expected_email
    ), "Expected and Actual email should be equal"
    assert new_state["tags"] == expected_tags
    assert new_state["parent_id"] == expected_parent_id

    # create account in real
    present_ret = await hub.states.aws.organizations.account.present(
        ctx,
        name=expected_account_name,
        email=expected_email,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        parent_id=expected_parent_id,
        tags=expected_tags,
    )
    new_state = present_ret.get("new_state")
    old_state = present_ret.get("old_state")

    assert new_state and not old_state

    assert (
        new_state["name"] == expected_account_name
    ), "Expected and Actual account_name should be equal"
    assert (
        new_state["email"] == expected_email
    ), "Expected and Actual email should be equal"
    assert new_state["tags"] == expected_tags
    assert new_state["parent_id"] == expected_parent_id

    list_accounts = await hub.exec.boto3.client.organizations.list_accounts_for_parent(
        ctx, ParentId=expected_parent_id
    )

    accounts = list_accounts["ret"].get("Accounts")

    filtered = list(
        filter(
            lambda acc: acc["Id"] == new_state["resource_id"]
            and acc["Arn"] == new_state["arn"],
            accounts,
        )
    )

    assert filtered
    assert filtered[0]["Id"] == new_state["resource_id"]
    assert filtered[0]["Arn"] == new_state["arn"]
    assert filtered[0]["Name"] == new_state["name"]
    assert filtered[0]["Email"] == new_state["email"]
    assert filtered[0]["Status"] == new_state["status"]

    # update account's parent id and tags

    list_roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)

    destination_parent_id = list_roots_resp["ret"]["Roots"][0]["Id"]

    updated_tags = [{"Key": "org", "Value": "idem-aws"}, {"Key": "env", "Value": "dev"}]

    # Update in test context
    update_ret = await hub.states.aws.organizations.account.present(
        test_ctx,
        name=new_state["name"],
        resource_id=new_state["resource_id"],
        email=expected_email,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        parent_id=destination_parent_id,
        tags=updated_tags,
    )

    await assert_account(
        hub,
        test_ctx,
        update_ret,
        expected_parent_id,
        expected_account_name,
        expected_email,
        updated_tags,
        destination_parent_id,
    )

    # Update in real
    update_ret = await hub.states.aws.organizations.account.present(
        ctx,
        name=new_state["name"],
        resource_id=new_state["resource_id"],
        email=expected_email,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        parent_id=destination_parent_id,
        tags=updated_tags,
    )

    await assert_account(
        hub,
        ctx,
        update_ret,
        expected_parent_id,
        expected_account_name,
        expected_email,
        updated_tags,
        destination_parent_id,
    )

    updated_tags_2 = [
        {"Key": "org", "Value": "idem-aws-acc"},
        {"Key": "env", "Value": "sandbox"},
    ]

    # Update in test context
    update_with_no_parent = await hub.states.aws.organizations.account.present(
        test_ctx,
        name=new_state["name"],
        resource_id=new_state["resource_id"],
        email=expected_email,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        tags=updated_tags_2,
    )

    await assert_account(
        hub,
        test_ctx,
        update_with_no_parent,
        destination_parent_id,
        expected_account_name,
        expected_email,
        updated_tags_2,
        destination_parent_id,
    )

    # Update in real
    update_with_no_parent = await hub.states.aws.organizations.account.present(
        ctx,
        name=new_state["name"],
        resource_id=new_state["resource_id"],
        email=expected_email,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        tags=updated_tags_2,
    )

    await assert_account(
        hub,
        ctx,
        update_with_no_parent,
        destination_parent_id,
        expected_account_name,
        expected_email,
        updated_tags_2,
        destination_parent_id,
    )

    # describe account

    describe_state = await hub.states.aws.organizations.account.describe(ctx)

    assert new_state["resource_id"] in describe_state
    state = describe_state.get(new_state["resource_id"]).get(
        "aws.organizations.account.present"
    )
    described_resource_map = dict(ChainMap(*state))
    assert destination_parent_id == described_resource_map["parent_id"]
    assert expected_account_name == described_resource_map["name"]
    assert expected_email == described_resource_map["email"]
    assert updated_tags_2 == described_resource_map["tags"]

    # remove account with test flag
    ret = await hub.states.aws.organizations.account.absent(
        test_ctx,
        name=update_with_no_parent.get("new_state")["name"],
        resource_id=update_with_no_parent.get("new_state")["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.organizations.account {expected_account_name}."
        in ret["comment"]
    )

    # remove account from organization
    delete_ret = await hub.states.aws.organizations.account.absent(
        ctx,
        name=update_with_no_parent.get("new_state")["name"],
        resource_id=update_with_no_parent.get("new_state")["resource_id"],
    )

    assert not delete_ret.get("new_state"), delete_ret.get("old_state")
    describe_after_delete = (
        await hub.states.aws.organizations.organization_unit.describe(ctx)
    )
    assert (
        update_with_no_parent.get("new_state")["resource_id"]
        not in describe_after_delete
    )


async def assert_account(
    hub,
    ctx,
    update_ret,
    expected_parent_id,
    expected_account_name,
    expected_email,
    updated_tags,
    destination_parent_id,
):
    before_update_state = update_ret.get("old_state")
    after_update_state = update_ret.get("new_state")

    assert before_update_state and after_update_state

    assert before_update_state["parent_id"] == expected_parent_id

    assert (
        after_update_state["name"] == expected_account_name
    ), "Expected and Actual account_name should be equal "
    assert (
        after_update_state["email"] == expected_email
    ), "Expected and Actual email should be equal"

    assert after_update_state["tags"] == updated_tags
    assert after_update_state["parent_id"] == destination_parent_id

    if not ctx.get("test", False):
        list_accounts_for_new_parent = (
            await hub.exec.boto3.client.organizations.list_accounts_for_parent(
                ctx, ParentId=destination_parent_id
            )
        )

        accounts = list_accounts_for_new_parent["ret"].get("Accounts")

        filtered_after_update = list(
            filter(
                lambda acc: acc["Id"] == after_update_state["resource_id"]
                and acc["Arn"] == after_update_state["arn"],
                accounts,
            )
        )

        assert filtered_after_update
        assert filtered_after_update[0]["Id"] == after_update_state["resource_id"]
        assert filtered_after_update[0]["Arn"] == after_update_state["arn"]
