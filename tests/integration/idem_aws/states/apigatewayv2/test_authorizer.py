import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_authorizer(hub, ctx, aws_apigatewayv2_api, aws_lambda_function):
    authorizer_temp_name = "idem-test-authorizer-" + str(int(time.time()))
    api_id = aws_apigatewayv2_api.get("api_id")
    authorizer_type = "REQUEST"
    identity_source = ["route.request.header.Auth"]
    new_identity_source = ["route.request.querystring.Name"]
    authorizer_uri = hub.tool.aws.arn_utils.build_arn(
        partition="aws",
        service="apigateway",
        region=ctx["acct"]["region_name"],
        account_id="lambda",
        resource="path/2015-03-31/functions/"
        + aws_lambda_function["function_arn"]
        + "/invocations",
    )

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create apigatewayv2 authorizer with test flag
    ret = await hub.states.aws.apigatewayv2.authorizer.present(
        test_ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        authorizer_type=authorizer_type,
        identity_source=identity_source,
        authorizer_uri=authorizer_uri,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.apigatewayv2.authorizer", name=authorizer_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert authorizer_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert authorizer_type == resource.get("authorizer_type")
    assert authorizer_uri == resource.get("authorizer_uri")
    assert identity_source == resource.get("identity_source")

    # Create apigatewayv2 authorizer
    ret = await hub.states.aws.apigatewayv2.authorizer.present(
        ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        authorizer_type=authorizer_type,
        identity_source=identity_source,
        authorizer_uri=authorizer_uri,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.apigatewayv2.authorizer", name=authorizer_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert authorizer_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert authorizer_type == resource.get("authorizer_type")
    assert authorizer_uri == resource.get("authorizer_uri")
    assert identity_source == resource.get("identity_source")

    resource_id = resource.get("resource_id")

    # Verify that the created apigatewayv2 authorizer is present (describe)
    ret = await hub.states.aws.apigatewayv2.authorizer.describe(ctx)

    assert authorizer_temp_name in ret
    assert "aws.apigatewayv2.authorizer.present" in ret.get(authorizer_temp_name)
    resource = ret.get(authorizer_temp_name).get("aws.apigatewayv2.authorizer.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("resource_id")
    assert authorizer_temp_name == resource_map.get("name")
    assert authorizer_type == resource_map.get("authorizer_type")
    assert authorizer_uri == resource_map.get("authorizer_uri")
    assert identity_source == resource_map.get("identity_source")

    # Create apigatewayv2 authorizer again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.apigatewayv2.authorizer.present(
        test_ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        authorizer_type=authorizer_type,
        identity_source=identity_source,
        resource_id=resource_id,
        authorizer_uri=authorizer_uri,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.authorizer",
            name=authorizer_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create apigatewayv2 authorizer again with same resource_id and no change in state
    ret = await hub.states.aws.apigatewayv2.authorizer.present(
        ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        authorizer_type=authorizer_type,
        identity_source=identity_source,
        resource_id=resource_id,
        authorizer_uri=authorizer_uri,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.authorizer",
            name=authorizer_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update apigatewayv2 authorizer with test flag
    ret = await hub.states.aws.apigatewayv2.authorizer.present(
        test_ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        authorizer_type=authorizer_type,
        identity_source=new_identity_source,
        resource_id=resource_id,
        authorizer_uri=authorizer_uri,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.authorizer",
            name=authorizer_temp_name,
        )[0]
        in ret["comment"]
    )
    assert f"Would update parameters: identity_source" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert authorizer_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert authorizer_type == resource.get("authorizer_type")
    assert authorizer_uri == resource.get("authorizer_uri")
    assert new_identity_source == resource.get("identity_source")

    # Update apigatewayv2 authorizer
    ret = await hub.states.aws.apigatewayv2.authorizer.present(
        ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        authorizer_type=authorizer_type,
        identity_source=new_identity_source,
        resource_id=resource_id,
        authorizer_uri=authorizer_uri,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.authorizer",
            name=authorizer_temp_name,
        )[0]
        in ret["comment"]
    )
    assert f"Updated parameters: identity_source" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert authorizer_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert authorizer_type == resource.get("authorizer_type")
    assert authorizer_uri == resource.get("authorizer_uri")
    assert new_identity_source == resource.get("identity_source")

    # Search for existing apigatewayv2 authorizer
    ret = await hub.states.aws.apigatewayv2.authorizer.search(
        ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert authorizer_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert authorizer_type == resource.get("authorizer_type")
    assert authorizer_uri == resource.get("authorizer_uri")
    assert new_identity_source == resource.get("identity_source")

    # Search for non-existing apigatewayv2 authorizer
    fake_resource_id = "fake-authorizer-" + str(int(time.time()))
    ret = await hub.states.aws.apigatewayv2.authorizer.search(
        ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        resource_id=fake_resource_id,
    )

    assert not ret["result"], ret["comment"]
    assert (
        f"Unable to find aws.apigatewayv2.authorizer resource with resource id '{fake_resource_id}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Delete apigatewayv2 authorizer with test flag
    ret = await hub.states.aws.apigatewayv2.authorizer.absent(
        test_ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.apigatewayv2.authorizer", name=authorizer_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert authorizer_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert authorizer_type == resource.get("authorizer_type")
    assert authorizer_uri == resource.get("authorizer_uri")
    assert new_identity_source == resource.get("identity_source")

    # Delete apigatewayv2 authorizer
    ret = await hub.states.aws.apigatewayv2.authorizer.absent(
        ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.apigatewayv2.authorizer", name=authorizer_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert authorizer_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert authorizer_type == resource.get("authorizer_type")
    assert authorizer_uri == resource.get("authorizer_uri")
    assert new_identity_source == resource.get("identity_source")

    # Delete the same apigatewayv2 authorizer again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.apigatewayv2.authorizer.absent(
        ctx,
        name=authorizer_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.authorizer", name=authorizer_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete apigatewayv2 authorizer with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.authorizer.absent(
        ctx, name=authorizer_temp_name, api_id=api_id
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.authorizer", name=authorizer_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
